package elemento;

import org.openqa.selenium.By;

public class Elemento {

	public By usuario = By.name("username");
	public By senha = By.name("pass");
	public By confirma = By.xpath("//button[@class='login100-form-btn']");
	public By novoCadastro = By.xpath("//a[@href='/empregados/new_empregado']");
	public By nome = By.id("inputNome");
	public By cpf = By.id("cpf");
	public By sexo = By.id("slctSexo");
	public By adimistracao = By.id("inputAdmissao");
	public By cargo = By.id("inputCargo");
	public By salario = By.id("dinheiro");
	public By contratação = By.id("clt");
	public By envia = By.xpath("//input[@type='submit']");
	public By verificarcadastro = By.xpath("//input[@type='search']");
	public By altera = By.xpath("//span[@class='fa fa-pencil']");
	public By contracao = By.id("pj");
	public By escluir = By.xpath("//span[@class='fa fa-trash']");
	public By ValidaTexto = By.xpath("//*[text()=' SUCESSO! ']");
}
