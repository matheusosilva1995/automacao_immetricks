package navegadores;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Navegadores {
	
	public static WebDriver driver;
	public void acessaNavegadores (String site, String navegador) {
		try {
			if (navegador.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "C:\\Driver\\driver96\\chromedriver.exe");
				driver = new ChromeDriver();
			}else if(navegador.equalsIgnoreCase("Firefox")){
				System.setProperty("webdriver.gecko.driver", "C:\\Driver\\firefox\\geckodriver.exe");
				driver = new FirefoxDriver();
			}else if(navegador.equalsIgnoreCase("Edge")){
				System.setProperty("webdriver.edge.driver", "C:\\Driver\\msedgedri\\msedgedriver.exe");
	            driver = new EdgeDriver();		
			}
			driver.get(site); 
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
		System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
		System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());

		}
	}

}
