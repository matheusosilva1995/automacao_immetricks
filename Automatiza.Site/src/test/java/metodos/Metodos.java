package metodos;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import navegadores.Navegadores;

public class Metodos extends Navegadores {

	public void click(By elemento) {
		try {
			driver.findElement(elemento).click();
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	} 

	public void Escrever(By elemento, String escrever) {
		try {
			driver.findElement(elemento).sendKeys(escrever);
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	}

	public void submit(By elemento) {
		try {
			driver.findElement(elemento).submit();
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	}

	public void Valida(String texto, By elemento) {
		try {
			String capturado = driver.findElement(elemento).getText();
			assertEquals(texto, capturado);
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	}

	public void validaJanela(By element) {
		try {
			int contador = 3;
			WebElement elemento = driver.findElement(element);
			while (elemento.isDisplayed() && contador > 0)
				elemento.click();
			contador--;
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	}

	public void screenShot(String foto) throws IOException {
		try {
			TakesScreenshot shot = ((TakesScreenshot) driver);
			File srcFile = shot.getScreenshotAs(OutputType.FILE);
			File desFile = new File("./src/evidencia/"+ foto +".png");
			FileUtils.copyFile(srcFile, desFile);
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	}

	public void excluirTexto(By elemento) {
		try {
			driver.findElement(elemento);
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	}

	public void tempo() {
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	}

	public void apagar(By elemento) {
		try {
			driver.findElement(elemento).clear();
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());
		}
	}
   
	public void fecharNavefador() {
		try {
			driver.quit();
		} catch (Exception e) {
			System.err.println("-------------------------Eroo ao digita-----------------------------" + e.getMessage());
			System.err.println("-------------------------Causa do erro------------------------------" + e.getCause());		}
	}
}
