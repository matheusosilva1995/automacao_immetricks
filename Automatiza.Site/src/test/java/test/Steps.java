package test;

import java.io.IOException;

import homepages.Homepages;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import navegadores.Navegadores;

public class Steps {

	Navegadores navegadores = new Navegadores();
	Homepages homes = new Homepages();

	@Given("que eue esteja {string}")
	public void que_eue_esteja(String site) {
		navegadores.acessaNavegadores(site, "Chrome");
	}

	@Then("cadastro um fucionario")
	public void cadastro_um_fucionario() {
		homes.CadastroDefucionario();

	}

	@Then("atualizo cadastro")
	public void atualizo_cadastro() {
		homes.altera();
	}

	@Then("Apago cadastro")
	public void apago_cadastro() {
		homes.exclui();
	}

	@When("valida mensagem de fucionario deletado")
	public void valida_mensagem_de_fucionario_deletado() {
		homes.validaTexto();
	}

	@Then("fecho o site")
	public void fecho_o_site() throws IOException {
		homes.fecharNavegador();
	}
}